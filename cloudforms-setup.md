# CloudForms Setup

There are 4 appliances, one dedicated DB in order to improve performance, 1 UI appliance, 1 Infra appliance and 1 OSP appliance with an extra filesystem dedicated to SSA in OSP.
3 zones has been created inside a single region

## Zone: Web UI

CF47-UI, provides Web UI for the entire CloudForms setup and is the only user interface, end users are supposed to directly interact with.

## Zone: CF Infra Providers

CF47-INF, dedicated appliance for the infrastructure providers, [vSphere](vsphere-setup.md) and [RHV](rhv-setup.md).

## Zone: CF OSP Provider

CF47-OSP, only provider in this Zone is [OpenStack](openstack-setup.md) in order to keep up the performance in this zone.