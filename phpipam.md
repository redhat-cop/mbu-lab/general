# phpIPAM

As a part of the blueprint we deploy [phpIPAM](https://phpipam.net/).

To allow API access, HTTPS is required. A Playbook can be found in the setup repository which automatically configures HTTPS and deployes some self signed certificates.
