# vSphere Setup

There is one vCenter server with 2 CPUs and 10 GB RAM running vCenter 6.7. Hostname for vCenter configured to 192.168.0.50 and storage controller in the VM has been modified to paravirtual

Two ESX nodes esxi1 and esxi2 each having 4 CPUs and 16 GB RAM. ESX Servers have been updated to 6.7 as well. 

NFS storage is mounted from [RH IDM](rhidm-setup.md).
