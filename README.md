# README

This repository contains instructions on how the Management Business Unit lab is build, which use case are prepared and how to demonstrate them.

Where applicable, it references to other repositories either in this Gitlab group or externally.

## WORK IN PROGRESS

Currently this is under heavy construction and should not be used unless you know what you're doing.

## Current blueprint

This is the current blueprint https://cloud.ravellosystems.com/#/0/library/blueprints/3125678670809/canvas with name GLOBAL-MBU-LAB-0.18

## Lab Overview

The lab is based on multiple components. The lab user is supposed to interact with them mostly by using the respective Web UI. SSH access is only possible by logging into the Bastion host and opening additional connections from there.

### Components

* Workstation: only machine accessible via SSH. When ordering the lab from the RHPDS catalog, your user will automatically be added and can SSH into this machine. From there, additional SSH connections can be opened to access all other systems

* CloudForms: One Region with several Zones is deployed. The CF UI appliance is used as the single point of contact. More details on the [CloudForms Setup](cloudform-setup.md).

* Satellite

* Ansible Tower

* Red Hat Insights

* Red Hat Virtualization: Version 4.2.4 with one Manager and two RHV Hypervisors. More details on the [RHV Setup](rhv-setup.md).

* VMware vSphere 6.7. More details on the [vSphere setup](vsphere-setup.md).

* Microsoft System Center Virtual Machine Manager. More details on the [SCVMM setup](scvmm-setup.md).

* Red Hat OpenStack Platform. More details on the [OpenStack setup](openstack-setup.md).

### Log in Credentials

Instructions on how to access the lab are automatically sent to the end user after ordering the lab from RHPDS. For administrative tasks on the blueprint itself, all usernames and passwords are stored in the [ansible-vault](ansible-vault) file. The Password to open this file should go into your local password file. If you want to use the [ansible.cfg](ansible.cfg) provided in the repository, store the password in the file ~/.vault-mbu on your local machine.

:warning: ***WARNING*** Never upload your password file to any git repository!